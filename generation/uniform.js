// generate a grid map with geoJSON format
function generateGridMapGeoJSON(collection, x1, y1, x2, y2) {
    var step_x = (x2 - x1) / 100.0;
    var step_y = (y2 - y1) / 100.0;

    collection.drop(); 
    collection.ensureIndex({loc: "2dsphere"});

    for( var i = x1; i < x2; ) {
        var bulk = collection.initializeUnorderedBulkOp();

        for(var j = y1; j < y2; ) {
            bulk.insert({loc: {type: "Point", coordinates: [i, j]}});
            j = j + step_y;
        }
        bulk.execute( {w: 1});
        i = i + step_x;
    }
    collection.getDB().getLastError();
}

generateGridMapGeoJSON(db.uniform_dense, -.005, -.005, .005, .005);
generateGridMapGeoJSON(db.uniform_sparse, 0, -90, 180, 90);
