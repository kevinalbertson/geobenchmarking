A small and simple suite of scripts to benchmark and perform sanity checks on geo queries in MongoDB.

- import.py - Imports and generates all necessary data into the benchmarking database
- index.py - (Re)indexes all necessary benchmarking collections
- benchmark.py - Runs benchmarks
- correctness.py - Performs sanity checks


## Correctness Test ##
To run a correctness test, you need to import and index the data. Once mongod is running

1. Run mongod on master (or your baseline setup)
2. Import with `python import.py`
3. Index with `python index.py`
4. Set the baseline `python correctness.py --reset`
5. Make any modifications to mongod necessary for testing
6. Run `python correctness.py`

Because some generation is random, if you re-import, you must reset the correctness test. In general, if you modify something regarding the index, you can just rerun index.py instead of re-importing.

