import benchmark
import os
import sys
from bson import json_util

def indexSize(collName, field, index_name="2dsphere"):
    command = "mongo benchmarking --quiet --eval \"print(tojson(%s))\""
    stats = "db.%s.stats()" % collName
    out = os.popen(command % stats).read()
    parsed = json_util.loads(out)
    return parsed["indexSizes"][field + "_" + index_name]

def recreateIndex(collectionName, field, test):
    command = "mongo benchmarking --eval \"%s\""
    drop = "db.%s.dropIndex({%s:'2dsphere'})" % (collectionName, field) 
    os.popen(command % drop).read()
    create = "db.%s.createIndex({%s:'2dsphere'},{coarsestIndexedLevel:%s,finestIndexedLevel:%s,maxCells:%s})" % (collectionName, field, test[0], test[1], test[2])
    os.popen(command % create).read()
'''
Run near queries from uniform grid over relevant areas
'''

def queryOver(lonMin, lonMax, latMin, latMax, collectionName, n=3):
    lon, lat = lonMin, latMin
    lonStep = (lonMax - lonMin) / n
    latStep = (latMax - latMin) / n
    avg = 0
    count = 0
    for i in range(n):
        lon += lonStep
        lat = latMin
        for j in range(n):
            lat += latStep
            point = [lon, lat]
            exp = benchmark.run_query(benchmark.near("geometry", point), collectionName, limit=5000, n=2)
            running_time = exp["executionStats"]["executionTimeMillis"]
            n_results = exp["executionStats"]["nReturned"]
            n_annuluses = len(exp["executionStats"]["executionStages"]["inputStage"]["searchIntervals"])
            avg += running_time
            count += 1
            print "Finished point (%s,%s) in %s millis with %s results over %s annuluses (%s/%s)" % (point[0], point[1], running_time, n_results, n_annuluses, count, n*n)
    avg /= n*n
    return avg

'''
Interested in how index finest/coarsest effects the following
on polygon data:

- Query execution time over realistic datasets
- Edge cases (large polygons, queries over humongous areas)
- Index size

We expect there to be a tradeoff in index size and performance:
Finer granularity -> better filtering but larger index
However, if polygons are indexed too finely, we'll likely lose performance
gains (since the same polygon will show up many many times)

These tests should be run with the query levels patch, as that will be more
representative of what the actual performance will be.

Default levels are: 7,14
'''

tests = [(7,16,50),(7,18,50),(7,20,50),(7,22,50),(7,24,50)]
summaries = []
for test in tests:
    summary = {
        "coarsest": test[0],
        "finest": test[1],
        "max_cells": test[2]
    }
    print "Reindexing NYC"
    recreateIndex("nyc", "geometry", test)
    summary["nyc_index_size"] = indexSize("nyc", "geometry")
    print "Querying over NYC"
    nyc_avg = queryOver(-74.003, -73.6655, 40.4957, 40.9280, "nyc", 3)
    print "Average running time %s" % nyc_avg

    print "Reindexing Rhode Island"
    recreateIndex("ri", "geometry", test)
    summary["ri_index_size"] = indexSize("ri", "geometry")
    print "Querying over Rhode Island"
    ri_avg = queryOver(-72.1555, -70.8717, 41.1583, 42.1878, "ri", 5)
    print "Average running time %s" % ri_avg
    summary["nyc_avg"] = nyc_avg
    summary["ri_avg"] = ri_avg
    summaries.append(summary)

print "Summary of results"
print "Coarsest,Finest,Max Cells,Avg Running Time NYC (ms),Index Size NYC,Avg Running Time RI (ms),Index Size RI"
for summary in summaries:
    print "%s,%s,%s,%s,%s,%s,%s" % (summary["coarsest"], summary["finest"], summary["max_cells"], summary["nyc_avg"], summary["nyc_index_size"], summary["ri_avg"], summary["ri_index_size"])
