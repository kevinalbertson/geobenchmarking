import numpy as np
import pymongo

def generate(dbname="test"):
	client = pymongo.MongoClient()
	db = client[dbname]
	mu, sigma = 0, 1
	points = np.random.normal(mu, sigma, (10000,2))
	for p in points:
		lon, lat = p[0] * 3, p[1] * 3
		pt = {
			"loc" : {
				"type" : "Point",
				"coordinates" : [lon,lat]
			}
		}
		db.gaussian.insert(pt)
