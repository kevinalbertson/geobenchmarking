import pymongo

def recreate_index(coll, field, index=pymongo.GEOSPHERE):
	print "Indexing %s" % coll.name
	param = (field, index)
	try:
		coll.drop_index([param])
	except:
		pass
	coll.create_index([param])

def main():
	dbname = "benchmarking"
	client = pymongo.MongoClient()
	db = client[dbname]
	recreate_index(db["gaussian"], "loc")
	recreate_index(db["uniform_dense"], "loc")
	recreate_index(db["uniform_sparse"], "loc")
	recreate_index(db["polygons"], "loc")
	recreate_index(db["parse"], "location")
	recreate_index(db["parse"], "_created_at", pymongo.ASCENDING)

	print "Done - run benchmark.py to see results"

if __name__ == "__main__":
	main()