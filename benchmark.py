from bson import json_util
import json
import os
import sys
import copy
import argparse

def _print_stats_headings():
    print "time,keysExamined,docsExamined,name"

def _print_stats(name, explain_data):
    global benchmark_name
    es = explain_data["executionStats"]
    print "%s, %s, %s, %s" % (es["executionTimeMillis"], es["totalKeysExamined"], es["totalDocsExamined"], benchmark_name + "_" + name)

def _record_results(name, explain_data, min_max=False, limit=False, side_length=False):
    rendered_name = name
    if min_max:
        rendered_name += "_MIN_%s_MAX_%s" % min_max
    if limit:
        rendered_name += "_LIMIT_%s" % limit
    if side_length:
        rendered_name += "_WITHIN_%s" % side_length

    f = open("explain/" + benchmark_name + "_" + rendered_name + ".json", 'w')
    f.write(json_util.dumps(explain_data))
    f.close()

    _print_stats(rendered_name, explain_data)

def run_query(query, collection, limit=None, n=10):
    """Runs a query and returns parsed JSON response.
    Takes the average of runtime of ten runs, after priming with one run.
    """
    tmpl = "db." + collection + ".find(%s)%s.explain(\"executionStats\")"
    limit_str = ""
    if limit:
        limit_str = ".limit(%s)" % limit
    query_str = tmpl % (json_util.dumps(query), limit_str)
    prepared_query = "print(tojson(%s))" % query_str
    s = "mongo benchmarking --quiet --eval '" + prepared_query + "'"
    total = 0
    returned = {}
    for i in range(n+1):
        returned = json_util.loads(os.popen(s).read())
        if i > 0:
            total += int(returned["executionStats"]["executionTimeMillis"])
    returned["executionStats"]["executionTimeMillis"] = total / n;
    return returned;

def benchmark_geo(name, collection, location_field, center_point, min_maxes=False, limits=False, side_lengths=False):
    """Benchmarks a specific geo query under multiple conditions
    Args:
        name: Base name of benchmark (used for unique identifier)
        collection: Name of collection to benchmark against
        location_field: Field of document which has 2dsphere index
        center_point: The point all geo queries are centered around
        min_maxes: List of tuples representing min/max distances to 
            test against
        limits: List of limit values to test against
        side_lengths: List of side lengths to test $within queries
    """

    near_query = near(location_field, center_point)

    if not limits and not min_maxes and not side_lengths:
        #run one $near without any
        _record_results(name, run_query(near_query, collection))

    if min_maxes:
        for min_max in min_maxes:
            near_query = _add_min_max(near_query, location_field, min_max)
            _record_results(name, run_query(near_query, collection), min_max, False, False)

    if limits:
        for l in limits:
            _record_results(name, run_query(near_query, collection, l), False, l, False)

    if side_lengths:
        for s in side_lengths:
            within_query = _within(location_field, center_point, s)
            _record_results(name, run_query(within_query, collection), False, False, s)


def _add_min_max(doc, location_field, min_max):
    doc_copy = copy.deepcopy(doc)
    doc_copy[location_field]["$near"]["$minDistance"] = min_max[0]
    doc_copy[location_field]["$near"]["$maxDistance"] = min_max[1]
    return doc_copy

def near(field, point):
    q = {}
    q[field] = {
        "$near" : {
            "$geometry" : {
                "type" : "Point",
                "coordinates" : point
            }
        }
    }
    return q

def _within(field, center_point, s):
    '''Gives a query with square centered at lon, lat, of side length 2s
    '''
    q = {}
    lon, lat = center_point
    geo = {
        "type" : "Polygon",
        "coordinates" : [[ [lon - s, lat - s], [lon + s, lat - s], [lon + s, lat + s], [lon - s, lat + s], [lon - s, lat - s]]]
    }
    q[field] = {}
    q[field]["$geoWithin"] = {}
    q[field]["$geoWithin"]["$geometry"] = geo
    return q

def main():
    if len(sys.argv) == 1:
        print "Usage: python benchmark.py <name> [-c <coarsest>] [-f <finest>] [--save_explains]"
        sys.exit(1)

    benchmark_name = sys.argv[1]

    coarsest, finest = False, False
    if len(sys.argv) == 4:
        coarsest, finest = int(sys.argv[2]), int(sys.argv[3])

    _print_stats_headings()
    default_limits = [1,10,100,1000,10000]
    default_min_maxes = [(.01, .1), (.1, 1), (1, 10), (10, 100)]
    default_side_lengths = [.0001, .001, .01, .1, 1]

    benchmark_geo("parse", "parse", "location", [106.6331, 10.7395], default_min_maxes, default_limits, default_side_lengths)
    benchmark_geo("uniform_dense", "uniform_dense", "loc", [0,0], default_min_maxes, default_limits, default_side_lengths)
    benchmark_geo("uniform_sparse", "uniform_sparse", "loc", [0,0], default_min_maxes, default_limits, default_side_lengths)
    benchmark_geo("gaussian_center", "gaussian", "loc", [0,0], default_min_maxes, default_limits, default_side_lengths)
    benchmark_geo("gaussian_offcenter", "gaussian", "loc", [2,2], default_min_maxes, default_limits, default_side_lengths)
    
if __name__ == "__main__":
    main()