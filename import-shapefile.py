import pymongo
import sys
import json
from pprint import pprint

'''
Imports a dataset into mongod similar to mongoimport.
The main distinction is that failure to insert invalid geometries
will not cause it to end inserting.
'''

def main():
    if len(sys.argv) != 3:
        print "Usage: python geoimport.py <collection> <input file>.json"
        sys.exit(1)

    client = pymongo.MongoClient()
    db = client.benchmarking
    coll = db[sys.argv[1]]

    print "Dropping collection"
    coll.drop()

    print "Creating 2dsphere index"
    coll.create_index([("geometry", pymongo.GEOSPHERE)])

    bulk = coll.initialize_unordered_bulk_op()
    infile = open(sys.argv[2])

    for line in infile:
        jsonObj = json.loads(line)
        bulk.insert(jsonObj)

    try:
        print "Inserting data"
        bulk.execute()
    except pymongo.errors.BulkWriteError as bwe:
        pprint(bwe.details)

    infile.close()

if __name__ == "__main__"
    main()