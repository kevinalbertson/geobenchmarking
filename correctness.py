from bson import json_util
import json
import os
import sys
import os.path

'''
This correctness test requires:
- all collections to be imported/generated (run import.py)
- all collections to be indexed (run index.py)

To set the baseline:
python index.py --reset

To test against the baseline:
python index.py
'''

class Correctness:
    def __init__(self, run_mode="check", baseline_filename="results/correct.csv"):
        self.run_mode = run_mode
        # maps test name to list of distances
        self.correct_map = {}
        # list of all checks
        self.tests = []

        self.baseline_file = None

        if run_mode == "check":
            print "Checking against baseline"
            if not os.path.isfile(baseline_filename):
                print "Could not find %s, run with --reset " % baseline_filename
                sys.exit(1)
            self.baseline_file = open(baseline_filename, "r")
            self._read_from_file()
        else:
            print "Resetting baseline"
            self.baseline_file = open(baseline_filename, "w")

        # threshold for two floating point numbers to be equal
        self.error_threshold = .00001

    def __del__(self):
        self.baseline_file.close()

    def add(self, collection, center_point, min_maxes, name=False):
        name = name if name else collection
        test = (collection, center_point, min_maxes, name)
        self.tests.append(test)

    def go(self):
        num_divergences = 0
        for test in self.tests:
            (collection, center_point, min_maxes, name) = test
            for min_max in min_maxes:
                full_name = name + "_MIN_%s_MAX_%s" % (min_max[0], min_max[1])
                dist_list = self._run_query(collection, center_point, min_max)
                if self.run_mode == "check":
                    if not self._check_correct(full_name, dist_list):
                        print "Divergence in test case: %s" % full_name
                        num_divergences += 1
                else:
                    self._write_to_file(full_name, dist_list)
        if self.run_mode == "check":
            print "# Divergences = %s" % num_divergences
        else:
            print "Baseline set, run without --reset to test"


    def _lists_equal(self, a,b):
        if len(a) != len(b):
            print "Unequal lists"
            print a, b
            return False
        for i in range(len(a)):
            if abs(a[i]-b[i]) > self.error_threshold:
                print "%s != %s" % (a[i], b[i])
                return False
        return True

    def _check_correct(self, name, dist_list):
        if name not in self.correct_map:
            print "%s not in existing correct measures" % name
            print "Run python correctness.py --reset"
            sys.exit(1)
        return self._lists_equal(dist_list, self.correct_map[name])

    def _write_to_file(self, full_name, dist_list):
        self.baseline_file.write("%s" % full_name)
        for d in dist_list:
            self.baseline_file.write(",%s" % d)
        self.baseline_file.write("\n")

    def _read_from_file(self):
        for line in self.baseline_file:
            if line[-1] == "\n":
                line = line[0:-1]
            parts = line.split(",")
            dist_list = []
            if len(parts) > 1:
                for p in parts[1:]:
                    dist_list.append(float(p))
            self.correct_map[parts[0]] = dist_list

    def _near(self, center_point):
        q = {
            "type" : "Point",
            "coordinates" : center_point
        }
        return json_util.dumps(q)

    def _run_query(self, collection, center_point, min_max):
        js_tmpl = """
        print(tojson(db.runCommand({ 
                aggregate : \"%s\", 
                pipeline: [
                {
                    $geoNear: { 
                            minDistance: %s, maxDistance: %s, 
                            spherical: true,
                            distanceField: \"distance\", near: %s
                         }
                 },
                 {
                    $project: {\"_id\" : 0, distance: 1}
                 }
            ]}
        )))"""
        query_str = self._near(center_point)
        js_str = js_tmpl % (collection, min_max[0], min_max[1], query_str)
        returned = os.popen("mongo benchmarking --quiet --eval '" + js_str + "'").read()
        data = json_util.loads(returned)
        dist_list = []
        for d in data["result"]:
            dist_list.append(d["distance"])
        return dist_list


def main():
    run_mode = "check"
    if len(sys.argv) == 2:
        if  sys.argv[1] == "-h" or sys.argv[1] == "--help":
            print "Usage: python correctness.py [--reset]"
            sys.exit(0)
        elif sys.argv[1] == "--reset":
            run_mode = "reset"

    min_maxes = [(0,.01), (.01, .1), (.1, 1), (1, 10), (10, 100), (100, 1000), (1000, 2000), (2000, 5000)]

    c = Correctness(run_mode=run_mode)
    c.add("parse", [106.6331, 10.7395], min_maxes)
    c.add("uniform_dense", [0.000329670329670329, 0.0009890109890109888], min_maxes)
    min_maxes.extend([(5000,10000),(10000, 30000)])
    c.add("gaussian", [0,0], min_maxes, name="gaussian_center")
    c.add("gaussian", [2,2], min_maxes, name="gaussian_offcenter")
    min_maxes.extend([(30000, 1000000), (1000000, 2000000)])
    c.add("uniform_sparse", [0, 0], min_maxes)
    c.add("polygons", [0, 0], min_maxes)
    c.go()


if __name__ == "__main__":
    main()
