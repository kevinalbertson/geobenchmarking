function genPoint(lon, lat){
    return [Math.random() * .2 - .1 + lon, Math.random() * .2 - .1 + lat];
}

function genPolygon(num_points, lon, lat){
    var polygon = {
        type : "Polygon"
    };
    var coordinates = [[]];
    var first = genPoint(lon, lat);
    coordinates[0].push(first);
    for(var i = 0; i < num_points; i++){
        coordinates[0].push(genPoint(lon, lat));
    }
    coordinates[0].push(first); //first and last must match
    polygon["coordinates"] = coordinates;
    return polygon;
}

function generateRandomPolygons(collection, lon, lat) {
    collection.drop(); 
    collection.ensureIndex({loc: "2dsphere"});

    for(var i = 0; i < 1000; i++){
        collection.insert({
            "loc" : genPolygon(3 + parseInt(i / 200), lon, lat),
        });
    }
}

generateRandomPolygons(db.polygons, 0, 0);
