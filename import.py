from subprocess import Popen
import generation.gaussian
import pymongo

print "Droping existing collections"
dbname = "benchmarking"
client = pymongo.MongoClient()
db = client[dbname]

print "Dropping database '%s'" % dbname
client.drop_database(dbname)

print "Importing parse dataset"
p = Popen("mongoimport -d %s -c parse --maintainInsertionOrder --jsonArray generation/2d-regression-date-range.json" % dbname, shell=True)
p.wait()

print "Generating uniform dataset"
p = Popen("mongo %s generation/uniform.js" % dbname, shell=True)
p.wait()

print "Generating gaussian dataset"
generation.gaussian.generate(dbname)

print "Generating polygons dataset"
p = Popen("mongo %s generation/polygons.js" % dbname, shell=True)
p.wait()


print "Done - Run index.py to create 2dsphere indexes"
